from BeautifulSoup import BeautifulSoup
import requests
import pickle
import threading
import sys

states = ['alabama', 'alaska', 'arizona', 'arkansas', 'california', 'colorado', 'connecticut', 'delaware', 'florida', 'georgia', 'hawaii', 'idaho', 'illinois', 'indiana', 'iowa', 'kansas', 'kentucky', 'louisiana', 'maine', 'maryland', 'massachusetts', 'michigan', 'minnesota', 'mississippi', 'missouri', 'montana', 'nebraska', 'nevada', 'new-hampshire', 'new-jersey', 'new-mexico', 'new-york', 'north-carolina', 'north-dakota', 'ohio', 'oklahoma', 'oregon', 'pennsylvania', 'rhode-island', 'south-carolina', 'south-dakota', 'tennessee', 'texas', 'utah', 'vermont', 'virginia', 'washington', 'west-virginia', 'wisconsin', 'wyoming', 'washington-dc']

def remove_non_ascii(text):
	return ''.join([i if ord(i) < 128 else ' ' for i in text])

def match_cities_to_states(states):
	sc_dict = {}
	for state in states:
		URL = "https://www.justia.com/lawyers/real-estate-law/{state}/all-cities"
		MATCH_URL = "/lawyers/real-estate-law/{state}/".format(state=state)
		bs = BeautifulSoup(requests.get(URL.format(state=state)).content)
		cities = [div.text for div in bs.findAll("a") if MATCH_URL in div["href"]]
		sc_dict[state] = cities
	pickle.dump(sc_dict, open("sc_dict.p", "w"))

sc_dict = pickle.load(open("sc_dict.p", "r"))

class Attorney(object):
	"""docstring for Attorney"""
	def __init__(self, name, city, state, phone, address, zip_code):
		super(Attorney, self).__init__()
		self.name = name
		self.city = city
		self.state = state
		self.phone = phone
		self.address = address
		self.zip_code = zip_code

class get_data(threading.Thread):
	def __init__ (self, cities, state):
		threading.Thread.__init__(self)
		self.cities = cities
		self.state = state

	def run(self):
		state = self.state

		for city in self.cities:
			city = city.lower()
			sys.stdout.write("Getting attornies for {}, {}\n".format(city, state))
			attornies = []
			has_next_page = True
			page = 1

			while has_next_page:
				if page > 1:
					URL = "https://www.justia.com/lawyers/real-estate-law/{state}/{city}?page={page}"
				else:
					URL = "https://www.justia.com/lawyers/real-estate-law/{state}/{city}"

				bs = BeautifulSoup(requests.get(URL.format(state=state, city=city, page=page)).content)

				for attorney_ in bs.findAll("div", {"class": "lawyer-summary -align-top"}):
					span = attorney_.find("span", {"itemprop": "name"})
					if span:
						name = span.text
						city = city
						state = state
						phone = attorney_.find("a", {"target": "_blank"})
						if phone:
							phone = phone.text
						address = attorney_.find("span", {"itemprop": "streetAddress"})
						if address:
							address = address.text
						zip_code = attorney_.find("span", {"itemprop": "postalCode"})
						if zip_code:
							zip_code = zip_code.text
						attorney_class = Attorney(name, city, state, phone, address, zip_code)
						attornies.append(attorney_class)
				has_next_page = len(bs.findAll("span", {"class": "next"})) > 0
				page += 1
				with open("attornies.csv", "a") as f:
					sys.stdout.write("Writing {} names from page {} of {}, {}\n".format(len(attornies), page, city, state))
					for a in attornies:
						f.write("{name},{phone},{address},{city},{state},{zip_code}\n".format(**vars(a)))
			attornies = []
			has_next_page = True
			page = 0

def chunkify(l, n):
	_chunks = []
	for i in range(0, len(l), n):
    		_chunks.append(l[i:i+n])
	return _chunks

threads = 100

# for state in sc_dict:
# 	num_of_cities = len(sc_dict[state])
# 	num_chunks = num_of_cities // threads
# 	# print num_of_cities, num_chunks
# 	if num_of_cities < threads:
# 		threads = num_of_cities
# 	chunks = chunkify(sc_dict[state], num_of_cities)
# 	# print len(chunks)
# 	# print chunks
# 	for chunk in chunks:
# 		thread = get_data(chunk, state)
# 		# print state, chunk
# 		thread.start()
# 		break
# 	# break
	
for state in sc_dict:
	cities = sc_dict[state]
	if state not in ['massachusetts', 'texas', 'washington-dc']:
		continue
	print "Getting attornies for {}".format(state)
	for city in cities:
		city = city.lower()
		attornies = []
		has_next_page = True
		page = 1

		while has_next_page:
			if page > 1:
				URL = "https://www.justia.com/lawyers/real-estate-law/{state}?page={page}"
			else:
				URL = "https://www.justia.com/lawyers/real-estate-law/{state}"

			get_url = URL.format(state=state, page=page)

			bs = BeautifulSoup(requests.get(get_url).content)
			for attorney_ in bs.findAll("div", {"class": "lawyer-summary -align-top"}):
				try:
					span = attorney_.find("span", {"itemprop": "name"})
					if span:
						name = span.text
						city = attorney_.find("span", {"itemprop": "addressLocality"})
						if city:
							city = city.text
						state = state
						phone = attorney_.find("a", {"target": "_blank"})
						if phone:
							phone = phone.text
						address = attorney_.find("span", {"itemprop": "streetAddress"})
						if address:
							address = address.text.replace(",", "")
						zip_code = attorney_.find("span", {"itemprop": "postalCode"})
						if zip_code:
							zip_code = zip_code.text
						attorney_class = Attorney(name, city, state, phone, address, zip_code)
						attornies.append(attorney_class)
				except Exception, e:
					print str(e)
			has_next_page = len(bs.findAll("span", {"class": "next"})) > 0
			with open("attornies.csv", "a") as f:
				print "Writing {} names from page {} of {}".format(len(attornies), page, state)
				for a in attornies:
					try:
						f.write("{name},{phone},{address},{city},{state},{zip_code}\n".format(**vars(a)))
					except Exception, e:
						print(e)
				attornies = []
			page += 1
		page = 0
		break