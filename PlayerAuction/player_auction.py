import requests, sys, urllib2, gzip
from StringIO import StringIO

from time import strftime, sleep
def get_time():
	return strftime("%H:%M:%S")

def clean(text):
	return ''.join([i if ord(i) < 128 else ' ' for i in text])

HEADERS = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}

HEADERS["Connection"] = "keep-alive"
HEADERS["Cache-Control"] = "max-age=0"
HEADERS["Origin"] = "https://www.playerauctions.com"
HEADERS["Upgrade-Insecure-Requests"] = "1"
HEADERS["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML] = like Gecko) Chrome/51.0.2704.103 Safari/537.36"
HEADERS["Content-Type"] = "application/x-www-form-urlencoded"
HEADERS["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
HEADERS["Referer"] = "https://www.playerauctions.com/member/signin/"
HEADERS["Accept-Encoding"] = "gzip, deflate, br"
HEADERS["Accept-Language"] = "en-US,en;q=0.8"
HEADERS["Cookie"] = "isFirstVist=True; ASP.NET_SessionId=zul2gh35djjxe545nyuym1mv; GLOBAL_COOKIE_ID=e72dc594-d83f-4498-bd3b-c80771542836; CountryISOCodeSetting=US; channel=google.com; __atssc=google%3B1; km_ai=DKFiP0G%2BEmSZq7b5hu%2FD0xYQP3M%3D; visid_incap_140605=hIHti4J6Sam6TN/yb4Cvj6JxmVcAAAAAQUIPAAAAAACCeDRTojkNj7eoTPDK7cug; incap_ses_209_140605=ZAyZdWsQ9nadqwcmKIXmApO1olcAAAAAot6sQHzZNtPHLmqQwFWqSA==; incap_ses_261_172126=2G1+JZYovn/DqavYm0OfA5W1olcAAAAAVxq3UltFV9Fiv8Hfs8KDuA==; km_lv=x; CurrencyUnitSetting=USD; visid_incap_172126=OsuoZrvgT9mFJiG7HP952qNxmVcAAAAAQUIPAAAAAADfiaNRzZa8DKybhXdBW5q0; incap_ses_208_172126=gz+MMVAZlmS1nEpqKffiAqm3olcAAAAAL9pWbznUR/UVQ9j0QixMvg==; __utmt=1; _gat_UA-3406877-1=1; userSurveyCookie=CurUserNickName=&IsOperate=false; __asc=fa6bff1115653954ad576ef9a84; __auc=872f9fb31562f63ec1152f46ba1; timeZone=-300; __utma=260853068.873181648.1469673897.1469673897.1470281111.2; __utmb=260853068.17.10.1470281111; __utmc=260853068; __utmz=260853068.1469673897.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __atuvc=33%7C30%2C16%7C31; __atuvs=57a2b59a5cc7941e00f; _ga=GA1.2.873181648.1469673897; kvcd=1470282406997; km_vs=1; km_uq=; incap_ses_261_140605=Nnr2E+WKVW+wK7rYm0OfA4O7olcAAAAA5Mgd+uGz0LN/6D6VDixn8A==; ___utmvmOYufwwF=TROLQlhXFfm; ___utmvbOYufwwF=hZu XhEOXalw: GtF; __utmli=ctl00_cphMian_btnSignIn"

import sys
username = ""#sys.argv[1]
password = ""#AjPc9l&dad"#sys.argv[2]
min_level = int(sys.argv[1])
list_amount = int(sys.argv[2])
with open("login.txt", "r") as f:
	f_ = f.read().split(":")
	username = f_[0]
	password = f_[1]
PROXY = #{"http": "http://209.242.141.60:8080"}# \
		# "https": "http://168.63.24.174:8134"}

SIGNIN_URL = "https://www.playerauctions.com/member/signin/"
s = requests.Session()
r = s.request(method="get", url=SIGNIN_URL, allow_redirects=True, headers=HEADERS, verify=True, proxies=PROXY)

while "__VIEWSTATE" not in r.text:
	r = s.request(method="get", url=SIGNIN_URL, allow_redirects=True, headers=HEADERS, verify=True, proxies=PROXY)
	print "[{}] Retrying signin".format(get_time())
	sleep(3)


data = {}
data["__VIEWSTATE"] = clean(r.text).split("id=\"__VIEWSTATE\" value=\"")[1].split("\"")[0]
data["__VIEWSTATEGENERATOR"] = clean(r.text).split("id=\"__VIEWSTATEGENERATOR\" value=\"")[1].split("\"")[0]
data["__LASTFOCUS"] = ""
data["__EVENTTARGET"] = ""
data["__EVENTARGUMENT"] = ""
data["ctl00$Top1$hfAdvancedSearch"] = ""
data["ctl00$Top1$hfHideMaster"] = ""
data["ctl00$Top1$hiddUserAccount"] = ""
data["ctl00$cphMian$txtUserID"] = username
data["ctl00$cphMian$txtPassword"] = password
data["ctl00$cphMian$btnSignIn"] = "Sign In"
r = s.post(url=r.url, data=data, allow_redirects=True, headers=HEADERS, verify=True, proxies=PROXY)
# print r.url
# with open("pa_login.html", "w") as f:
# 	f.write(clean(r.text))

POST_URL = "https://www.playerauctions.com/member/offereditor/?type=sell"
r = s.request(method="get", url=POST_URL, allow_redirects=True, verify=True, proxies=PROXY)
# print r.url
# with open("pa_sell.html", "w") as f:
# 	f.write(clean(r.text))

accounts = []
already_posted = []
with open("ptc_accounts.txt", "r") as f:
	accounts = f.read().split("\n")
with open("ptc_accounts_posted.txt", "r") as f:
	already_posted = f.read()
	# print already_posted
# if 1==1:
# 	sys.exit()
# print "[{}] {} accounts".format(get_time(), len(accounts))
# removed = 0
# for _ in accounts:
# 	if _.split(" ")[0] in already_posted:
# 		print "removing", _
# 		removed += 1
# 		accounts.remove(_)
# print "[{}] Removed {} already posted accounts".format(get_time(), removed)
# accounts = list(set(accounts) - set(already_posted))
# print "[{}] {} accounts".format(get_time(), len(accounts))
# accounts = accounts[:list_amount]
print "[{}] {} accounts".format(get_time(), len(accounts))
print "[{}] Starting with: '{}'".format(get_time(), accounts[0])
num_posted = 0
for account in accounts:
	try:
		BODY = """<div style="margin: 0px; 0px; 0px; inherit; 12px; font-family: Arial, Tahoma, Verdana, Helvetica, &quot;Microsoft YaHei&quot;, serif, sans-serif, monospace; font-size: 12px; vertical-align: baseline; transition: color 0.2s ease-in-out, background-color 0.2s ease-in-out, border-color 0.2s ease-in-out; color: #bcbcbc; background-color: #545454;"><strong style="margin: 0px; 0px; 0px; font-style: inherit; inherit; inherit; inherit; font-family: inherit; font-size: 14pt; vertical-align: baseline; background-color: yellow;"></strong></div>
		<p style="vertical-align: baseline; background: #545454;"><br />
		</p>
		<p style="vertical-align: baseline; color: #ffffff; background: #545454;"><span style="font-size:14.0pt;font-family:&quot;Arial&quot;,sans-serif; &quot;Times New Roman&quot;;color:white">Top 5:</span></p>
		<p style="vertical-align: baseline; color: #ffffff; background: #545454;"><strong><span style="font-size: 14.0pt;font-family:&quot;inherit&quot;,serif;&quot;Times New Roman&quot;;Arial;color:white"></span></strong></p>
		<div id="pastingspan1" style="color: #ffffff;"><span style="font-size: 14pt; color: #ffffff;"><strong></strong></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="color: #ffffff; font-size: 14pt;"><strong></strong></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="color: #ffffff; font-size: 14pt;"><strong></strong></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="font-size: 14pt;"><strong></strong></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="color: #ffffff; font-size: 14pt;"><strong></strong></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="font-size: 14pt;"></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="color: #ffffff; font-size: 14pt;"></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="font-size: 14pt;"></span></div>
		<div><span style="color: #ffffff; font-size: 14pt;"><strong>{}</strong></span></div>
		<div style="color: #ffffff; font-size: 14pt;">{}</div>
		<div style="color: #ffffff; font-size: 14pt;">{}</div>
		<div style="color: #ffffff; font-size: 14pt;">{}</div>
		<div><span style="color: #ffffff; font-size: 14pt;">{}</span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="font-size: 14pt;"></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="color: #ffffff;"></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="font-size: 14pt;"></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="color: #ffffff;"></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="font-size: 14pt;"></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="color: #ffffff;"></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="color: #ffffff;"></span></div>
		<div id="pastingspan1" style="color: #ffffff;"><span style="font-size: 14pt;"></span></div>
		<p style="vertical-align: baseline; color: #ffffff; background: #545454;"><span style="font-size:14.0pt; font-family:&quot;Arial&quot;,sans-serif;&quot;Times New Roman&quot;; color:white"></span></p>
		<p style="vertical-align: baseline; color: #ffffff; background: #545454;"><span style="font-size:18.0pt; font-family:&quot;inherit&quot;,serif;&quot;Times New Roman&quot;;Arial;color:white">Note: Also Has Hundreds of Candies! </span><span style="font-size:18.0pt;font-family:&quot;Arial&quot;,sans-serif;&quot;Times New Roman&quot;;color:white">You will be able to set the nickname after login.</span></p>
		<p style=" 0.0001pt; vertical-align: baseline; background: #545454;"><span style="font-size: 18pt; font-family: inherit, serif; color: #ffffff;">It has not yet been assigned to a team either! (:</span> </p>
		<div style="margin: 0px; 0px; 0px; inherit; 12px; font-family: Arial, Tahoma, Verdana, Helvetica, &quot;Microsoft YaHei&quot;, serif, sans-serif, monospace; font-size: 12px; vertical-align: baseline; transition: color 0.2s ease-in-out, background-color 0.2s ease-in-out, border-color 0.2s ease-in-out; color: #bcbcbc; background-color: #545454;"><span style="margin: 0px; font-style: inherit; font-weight: inherit; font-family: inherit; font-size: 18pt; vertical-align: baseline; color: #ffffff;"></span></div>"""
		# BODY = BODY.format()

		ACCOUNT_USERNAME = account.split(" ")[0]
		ACCOUNT_PASSWORD = account.split(" ")[1]
		if ACCOUNT_USERNAME.lower() in already_posted.lower():
			print "[{}] Skipping '{}', already posted".format(get_time(), ACCOUNT_USERNAME)
			continue
		DATA_URL = "http://142.4.204.114/data/{}/{}".format(ACCOUNT_USERNAME, ACCOUNT_PASSWORD)
		s_ = requests.Session()
		r_ = s_.request(method="get", url=DATA_URL, allow_redirects=True, headers=HEADERS, verify=True, proxies=PROXY)
		response = r_.text
		i = 0
		while "internal error" in response:
			# print "[{}] Trying to get data for {}".format(get_time(), ACCOUNT_USERNAME)
			r_ = s_.request(method="get", url=DATA_URL, allow_redirects=True, headers=HEADERS, verify=True, proxies=PROXY)
			response = r_.text
			from time import sleep
			sleep(2)
			i = i + 1
			if i >= 5:
				print "[{}] Skipping, took over 5 tries for {}".format(get_time(), ACCOUNT_USERNAME)
				continue
		# print response
		level, first, second, third, fourth, fifth = response.split("<br />")
		BODY = BODY.format(first, second, third, fourth, fifth)
		if int(level) < int(min_level):
			print "[{}] Level {} is too low for {}".format(get_time(), level, ACCOUNT_USERNAME)
			with open("ptc_accounts_posted.txt", "a") as f:
				f.write(ACCOUNT_USERNAME + "\n")
			continue
		TITLE = "Level {} {} w/ {} + Choose your nickname".format(level, first.title().replace("Cp", "CP"), second.title().replace("Cp", "CP"))
		# print level, first, second, third, fourth, fifth
		# print TITLE
		data = {}
		data["__EVENTTARGET"] = ""
		data["__EVENTARGUMENT"] = ""
		data["__LASTFOCUS"] = ""
		data["__VIEWSTATE"] = clean(r.text).split("id=\"__VIEWSTATE\" value=\"")[1].split("\"")[0]
		data["ctl00$WebUserControl5$hfAdvancedSearch"] = ""
		data["ctl00$WebUserControl5$hfHideMaster"] = ""
		data["ctl00$WebUserControl5$hiddUserAccount"] = username
		data["ctl00$cphMain$hfCDkeyId"] = "5325"
		data["ctl00$cphMain$hfGiftCardsId"] = "6672"
		data["ctl00$cphMain$rblProductType"] = "3"
		data["ctl00$cphMain$ddlGame"] = "7464"
		data["ctl00$cphMain$ddlGameCopy"] = ""
		data["ctl00$cphMain$txtCategoryId"] = "7466"
		data["ctl00$cphMain$txtLoginName2"] = ACCOUNT_USERNAME
		data["ctl00$cphMain$txtReLoginName2"] = ACCOUNT_USERNAME
		data["ctl00$cphMain$txtBatLoginName"] = ""
		data["ctl00$cphMain$txtLoginName"] = ""
		data["ctl00$cphMain$txtBatRetypeLoginName"] = ""
		data["ctl00$cphMain$txtReLoginName"] = ""
		data["ctl00$cphMain$iptSteamID"] = "long-form SteamID"
		data["ctl00$cphMain$hidSteamResult"] = "fail"
		data["ctl00$cphMain$rbdurationlistForCurrency"] = "7"
		data["ctl00$cphMain$rbdurationlistForOther"] = "30"
		data["ctl00$cphMain$txtCurrencyPerUnit"] = ""
		data["ctl00$cphMain$txtTotalUnits"] = ""
		data["ctl00$cphMain$txtMinimumSell"] = ""
		data["ctl00$cphMain$txtUnitPrice"] = ""
		data["ctl00$cphMain$txtQuantity"] = ""
		data["ctl00$cphMain$txtPrice"] = "5"
		data["ctl00$cphMain$txtEndPrice"] = ""
		data["ctl00$cphMain$rblEnableReduction"] = "1"
		data["ctl00$cphMain$txtWaitDays"] = "5"
		data["ctl00$cphMain$txtSaleToPA"] = ""
		data["ctl00$cphMain$ListingPrice"] = "5"
		data["ctl00$cphMain$PayoutAmount"] = "3.41"
		data["ctl00$cphMain$rblist_1"] = "2"
		data["ctl00$cphMain$hfimgAccountInfo1"] = ""
		data["ctl00$cphMain$hfimgAccountInfoRelative1"] = ""
		data["ctl00$cphMain$hfimgAccountInfo2"] = ""
		data["ctl00$cphMain$hfimgAccountInfoRelative2"] = ""
		data["ctl00$cphMain$hfimgAccountInfo3"] = ""
		data["ctl00$cphMain$hfimgAccountInfoRelative3"] = ""
		data["ctl00$cphMain$hfimgAccountInfo4"] = ""
		data["ctl00$cphMain$hfimgAccountInfoRelative4"] = ""
		data["ctl00$cphMain$hfimgIdCopy"] = ""
		data["ctl00$cphMain$hfimgIdCopyRelative"] = ""
		data["ctl00$cphMain$rblist_14"] = "2"
		data["ctl00$cphMain$rblDisburseDelayDays"] = "7"
		data["ctl00$cphMain$rblist_12"] = "0"
		data["ctl00$cphMain$rblist_2"] = "1"
		data["ctl00$cphMain$rblist_3"] = "1"
		data["ctl00$cphMain$rblist_4"] = "0"
		data["ctl00$cphMain$rblist_15"] = "1"
		data["ctl00$cphMain$rblist_6"] = "1"
		data["ctl00$cphMain$rblist_13"] = "0"
		data["ctl00$cphMain$rblist_7"] = "1"
		data["ctl00$cphMain$rblist_8"] = "1"
		data["ctl00$cphMain$rblist_9"] = "1"
		data["ctl00$cphMain$rblist_10"] = "1"
		data["ctl00$cphMain$DescribeMainCharacter$ddlClass"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$ddlRace"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtLevel"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$ddlEquipmentQuality"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$ddlEquipmentQuality2"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtCharacterHP"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtCharacterMP"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtPet1"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtPet2"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtTitle1"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtTitle2"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtJusticePoints"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtValorPoints"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtConquestPoints"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtHonorPoints"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtPrice"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtEndGameTime"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtProfileLink"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$rblLandMount"] = "1"
		data["ctl00$cphMain$DescribeMainCharacter$rblFlyingMount"] = "1"
		data["ctl00$cphMain$DescribeMainCharacter$rblUnderwaterMount"] = "1"
		data["ctl00$cphMain$DescribeMainCharacter$txtRareMount1"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtRareMount2"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$txtRareMount3"] = ""
		data["ctl00$cphMain$DescribeMainCharacter$ddlAmount"] = "0"
		data["ctl00$cphMain$DescribeMainCharacter$ddlAccountExpansion"] = "1"
		data["ctl00$cphMain$DescribeMainCharacter$hfQualityValue"] = "1"
		data["ctl00$cphMain$DescribeMainCharacter$hfQualityValue2"] = "1"
		data["ctl00$cphMain$DescribeMainCharacter$hfIsMainCharacter"] = "1"
		data["ctl00$cphMain$DescribeMainCharacter$hfMainServerName"] = ""
		data["ctl00$cphMain$CharacterDescribeUCtrol1$ddlClass"] = ""
		data["ctl00$cphMain$CharacterDescribeUCtrol1$txtLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeUCtrol1$txtParagonLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeUCtrol1$hfQualityValue"] = "1"
		data["ctl00$cphMain$CharacterDescribeUCtrol1$hfQualityValue2"] = "1"
		data["ctl00$cphMain$CharacterDescribeUCtrol1$hfIsMainCharacter"] = "1"
		data["ctl00$cphMain$CharacterDescribeUCtrol1$hfMainServerName"] = ""
		data["ctl00$cphMain$CharacterDescribeForGW2UCtrol1$ddlClass"] = ""
		data["ctl00$cphMain$CharacterDescribeForGW2UCtrol1$ddlRace"] = ""
		data["ctl00$cphMain$CharacterDescribeForGW2UCtrol1$txtLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeForGW2UCtrol1$hfQualityValue"] = "1"
		data["ctl00$cphMain$CharacterDescribeForGW2UCtrol1$hfQualityValue2"] = "1"
		data["ctl00$cphMain$CharacterDescribeForGW2UCtrol1$hfIsMainCharacter"] = "1"
		data["ctl00$cphMain$CharacterDescribeForGW2UCtrol1$hfMainServerName"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$ddlServer"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$ddlClass"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$ddlRace"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$txtLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$txtProfileLink"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$ddlAmount"] = "0"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$txtGameId"] = "7464"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$txtGameName"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$txtMaxCharacterLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$hfQualityValue"] = "1"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$hfQualityValue2"] = "1"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$hfIsMainCharacter"] = "1"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame$hfMainServerName"] = ""
		data["ctl00$cphMain$GameCharacterRS1$txtLevel"] = ""
		data["ctl00$cphMain$GameCharacterRS1$ddlType"] = ""
		data["ctl00$cphMain$GameCharacterRS1$txtGold"] = ""
		data["txtskill_1"] = ""
		data["txtskill_2"] = ""
		data["txtskill_3"] = ""
		data["txtskill_4"] = ""
		data["txtskill_5"] = ""
		data["txtskill_6"] = ""
		data["txtskill_7"] = ""
		data["txtskill_8"] = ""
		data["txtskill_9"] = ""
		data["txtskill_10"] = ""
		data["txtskill_11"] = ""
		data["txtskill_12"] = ""
		data["txtskill_13"] = ""
		data["txtskill_14"] = ""
		data["txtskill_15"] = ""
		data["txtskill_16"] = ""
		data["txtskill_17"] = ""
		data["txtskill_18"] = ""
		data["txtskill_19"] = ""
		data["txtskill_20"] = ""
		data["txtskill_21"] = ""
		data["txtskill_22"] = ""
		data["txtskill_23"] = ""
		data["txtskill_24"] = ""
		data["txtskill_25"] = ""
		data["txtskill_26"] = ""
		data["txtskill_27"] = ""
		data["txtskill_28"] = ""
		data["txtskill_29"] = ""
		data["txtequipments_303"] = ""
		data["txtequipments_304"] = ""
		data["txtequipments_305"] = ""
		data["txtequipments_306"] = ""
		data["txtequipments_307"] = ""
		data["txtequipments_308"] = ""
		data["txtequipments_309"] = ""
		data["txtequipments_310"] = ""
		data["txtequipments_311"] = ""
		data["txtequipments_312"] = ""
		data["txtequipments_313"] = ""
		data["txtequipments_314"] = ""
		data["txtequipments_315"] = ""
		data["txtequipments_316"] = ""
		data["txtequipments_317"] = ""
		data["txtequipments_318"] = ""
		data["txtequipments_319"] = ""
		data["txtequipments_320"] = ""
		data["txtequipments_321"] = ""
		data["txtequipments_322"] = ""
		data["txtequipments_323"] = ""
		data["txtequipments_324"] = ""
		data["txtequipments_325"] = ""
		data["txtequipments_326"] = ""
		data["txtequipments_327"] = ""
		data["txtequipments_328"] = ""
		data["txtequipments_329"] = ""
		data["txtequipments_330"] = ""
		data["txtequipments_340"] = ""
		data["txtequipments_341"] = ""
		data["txtequipments_730"] = ""
		data["txtequipments_731"] = ""
		data["txtequipments_732"] = ""
		data["txtequipments_735"] = ""
		data["txtequipments_736"] = ""
		data["txtequipments_737"] = ""
		data["txtequipments_738"] = ""
		data["txtequipments_739"] = ""
		data["txtequipments_740"] = ""
		data["txtequipments_741"] = ""
		data["txtequipments_742"] = ""
		data["txtequipments_743"] = ""
		data["txtequipments_744"] = ""
		data["txtequipments_745"] = ""
		data["txtequipments_746"] = ""
		data["txtequipments_747"] = ""
		data["txtequipments_748"] = ""
		data["txtequipments_749"] = ""
		data["txtequipments_750"] = ""
		data["txtequipments_751"] = ""
		data["txtequipments_752"] = ""
		data["txtequipments_753"] = ""
		data["txtequipments_754"] = ""
		data["txtequipments_755"] = ""
		data["txtequipments_756"] = ""
		data["txtequipments_757"] = ""
		data["txtequipments_758"] = ""
		data["txtequipments_824"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$ddlServer"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$ddlClass"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$ddlRace"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtLevel"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$ddlEquipmentQuality"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$ddlEquipmentQuality2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtCharacterHP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtCharacterMP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtPet1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtPet2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtTitle1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtTitle2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtJusticePoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtValorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtConquestPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtHonorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtPrice"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtEndGameTime"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtProfileLink"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$rblLandMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter1$rblFlyingMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter1$rblUnderwaterMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter1$txtRareMount1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtRareMount2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$txtRareMount3"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter1$ddlAmount"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter1$hfQualityValue"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter1$hfQualityValue2"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter1$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter1$hfMainServerName"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$ddlServer"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$ddlClass"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$ddlRace"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$txtLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$txtProfileLink"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$ddlAmount"] = "0"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$txtGameId"] = "7464"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$txtGameName"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$txtMaxCharacterLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$hfQualityValue"] = "1"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$hfQualityValue2"] = "1"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$hfMainServerName"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$ddlServer"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$ddlClass"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$ddlRace"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtLevel"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$ddlEquipmentQuality"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$ddlEquipmentQuality2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtCharacterHP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtCharacterMP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtPet1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtPet2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtTitle1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtTitle2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtJusticePoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtValorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtConquestPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtHonorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtPrice"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtEndGameTime"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtProfileLink"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$rblLandMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter2$rblFlyingMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter2$rblUnderwaterMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter2$txtRareMount1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtRareMount2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$txtRareMount3"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter2$ddlAmount"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter2$hfQualityValue"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter2$hfQualityValue2"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter2$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter2$hfMainServerName"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$ddlServer"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$ddlClass"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$ddlRace"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$txtLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$txtProfileLink"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$ddlAmount"] = "0"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$txtGameId"] = "7464"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$txtGameName"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$txtMaxCharacterLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$hfQualityValue"] = "1"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$hfQualityValue2"] = "1"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$hfMainServerName"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$ddlServer"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$ddlClass"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$ddlRace"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtLevel"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$ddlEquipmentQuality"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$ddlEquipmentQuality2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtCharacterHP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtCharacterMP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtPet1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtPet2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtTitle1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtTitle2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtJusticePoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtValorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtConquestPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtHonorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtPrice"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtEndGameTime"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtProfileLink"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$rblLandMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter3$rblFlyingMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter3$rblUnderwaterMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter3$txtRareMount1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtRareMount2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$txtRareMount3"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter3$ddlAmount"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter3$hfQualityValue"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter3$hfQualityValue2"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter3$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter3$hfMainServerName"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$ddlServer"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$ddlClass"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$ddlRace"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$txtLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$txtProfileLink"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$ddlAmount"] = "0"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$txtGameId"] = "7464"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$txtGameName"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$txtMaxCharacterLevel"] = ""
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$hfQualityValue"] = "1"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$hfQualityValue2"] = "1"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$hfMainServerName"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$ddlServer"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$ddlClass"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$ddlRace"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtLevel"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$ddlEquipmentQuality"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$ddlEquipmentQuality2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtCharacterHP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtCharacterMP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtPet1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtPet2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtTitle1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtTitle2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtJusticePoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtValorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtConquestPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtHonorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtPrice"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtEndGameTime"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtProfileLink"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$rblLandMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter4$rblFlyingMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter4$rblUnderwaterMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter4$txtRareMount1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtRareMount2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$txtRareMount3"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter4$ddlAmount"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter4$hfQualityValue"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter4$hfQualityValue2"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter4$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter4$hfMainServerName"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$ddlServer"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$ddlClass"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$ddlRace"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtLevel"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$ddlEquipmentQuality"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$ddlEquipmentQuality2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtCharacterHP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtCharacterMP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtPet1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtPet2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtTitle1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtTitle2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtJusticePoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtValorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtConquestPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtHonorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtPrice"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtEndGameTime"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtProfileLink"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$rblLandMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter5$rblFlyingMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter5$rblUnderwaterMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter5$txtRareMount1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtRareMount2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$txtRareMount3"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter5$ddlAmount"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter5$hfQualityValue"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter5$hfQualityValue2"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter5$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter5$hfMainServerName"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$ddlServer"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$ddlClass"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$ddlRace"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtLevel"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$ddlEquipmentQuality"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$ddlEquipmentQuality2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtCharacterHP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtCharacterMP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtPet1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtPet2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtTitle1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtTitle2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtJusticePoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtValorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtConquestPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtHonorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtPrice"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtEndGameTime"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtProfileLink"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$rblLandMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter6$rblFlyingMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter6$rblUnderwaterMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter6$txtRareMount1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtRareMount2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$txtRareMount3"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter6$ddlAmount"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter6$hfQualityValue"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter6$hfQualityValue2"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter6$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter6$hfMainServerName"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$ddlServer"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$ddlClass"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$ddlRace"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtLevel"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$ddlEquipmentQuality"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$ddlEquipmentQuality2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtCharacterHP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtCharacterMP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtPet1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtPet2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtTitle1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtTitle2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtJusticePoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtValorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtConquestPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtHonorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtPrice"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtEndGameTime"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtProfileLink"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$rblLandMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter7$rblFlyingMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter7$rblUnderwaterMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter7$txtRareMount1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtRareMount2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$txtRareMount3"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter7$ddlAmount"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter7$hfQualityValue"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter7$hfQualityValue2"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter7$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter7$hfMainServerName"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$ddlServer"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$ddlClass"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$ddlRace"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtLevel"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$ddlEquipmentQuality"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$ddlEquipmentQuality2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtCharacterHP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtCharacterMP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtPet1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtPet2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtTitle1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtTitle2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtJusticePoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtValorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtConquestPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtHonorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtPrice"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtEndGameTime"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtProfileLink"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$rblLandMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter8$rblFlyingMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter8$rblUnderwaterMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter8$txtRareMount1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtRareMount2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$txtRareMount3"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter8$ddlAmount"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter8$hfQualityValue"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter8$hfQualityValue2"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter8$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter8$hfMainServerName"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$ddlServer"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$ddlClass"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$ddlRace"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtLevel"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$ddlEquipmentQuality"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$ddlEquipmentQuality2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtCharacterHP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtCharacterMP"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtPet1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtPet2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtTitle1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtTitle2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtJusticePoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtValorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtConquestPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtHonorPoints"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtPrice"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtEndGameTime"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtProfileLink"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$rblLandMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter9$rblFlyingMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter9$rblUnderwaterMount"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter9$txtRareMount1"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtRareMount2"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$txtRareMount3"] = ""
		data["ctl00$cphMain$DescribeExtraCharacter9$ddlAmount"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter9$hfQualityValue"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter9$hfQualityValue2"] = "1"
		data["ctl00$cphMain$DescribeExtraCharacter9$hfIsMainCharacter"] = "0"
		data["ctl00$cphMain$DescribeExtraCharacter9$hfMainServerName"] = ""
		data["ctl00$cphMain$txtTitle"] = TITLE
		data["ctl00$cphMain$hfScreenshot"] = "/20160803/948eb74882b3396a4093e22e55c6ddeb.png"
		data["ctl00$cphMain$hfScreenshotUrl"] = "http://image.playerauctions.com/P/B/20160803/948eb74882b3396a4093e22e55c6ddeb.png"
		data["ctl00$cphMain$Editor1"] = BODY
		data["ctl00$cphMain$Editor1$ClientState"] = "ToggleBorder=b:true;FullPage=b:false"
		data["ctl00$cphMain$Editor1$PostBackHandler"] = ""
		data["addImageTo"] = "on"
		data["ctl00$cphMain$hfSlideshowUrl1"] = ""
		data["ctl00$cphMain$hfSlideshowRelativeUrl1"] = ""
		data["ctl00$cphMain$hfSlideshowUrl2"] = ""
		data["ctl00$cphMain$hfSlideshowRelativeUrl2"] = ""
		data["ctl00$cphMain$hfSlideshowUrl3"] = ""
		data["ctl00$cphMain$hfSlideshowRelativeUrl3"] = ""
		data["ctl00$cphMain$hfSlideshowUrl4"] = ""
		data["ctl00$cphMain$hfSlideshowRelativeUrl4"] = ""
		data["ctl00$cphMain$rblDeliveryGuarantee"] = "6"
		data["Input_CommitDelivTimeInHour"] = ""
		data["minvalue"] = ""
		data["maxvalue"] = ""
		data["minvalue"] = ""
		data["maxvalue"] = ""
		data["minvalue"] = ""
		data["maxvalue"] = ""
		data["minvalue"] = ""
		data["maxvalue"] = ""
		data["ctl00$cphMain$txtCharacterName"] = ""
		data["ctl00$cphMain$hfCategoryId"] = "7466"
		data["ctl00$cphMain$txtDeliveryInstruction"] = "(optional)"
		data["ctl00$cphMain$chkAgreement"] = "on"
		data["ctl00$cphMain$btnCreate"] = "Create New For Sale Offer"
		data["ctl00$cphMain$hidisedit"] = ""
		data["ctl00$cphMain$hidProduct"] = "3"
		data["ctl00$cphMain$hidIsRecommend"] = "false"
		data["ctl00$cphMain$hdProductType_1"] = "4883,7388,2801,5890,3376,3134,6664,7,4964,4957,5659,6058,7375,2813,5974,5894,6662,6364,6660,7482,3217,5130,5128,3229,6774,2307,5579,7462,7372,3712,7116,3450,7071,22,4130,4178,3440,17,14,6917,6091,7113,6778,5942,5698,5997,8,4,11,3538,2818,7181,2,3647,5373,24,5901,7418,5917,13,5135,5052,2785,2853,2855,3,28,27,2884,7027,7423,7109,6658,29,2890,4173,7179,3550,3548,3020,7161,7452,3132,5583,7166,7489,7458,5073,6393,5243,6925,2928,3703,5374,5767,31,5538,2938,7157,7444,5780,3743,5204,33,7095,5567,9,34,5713,6977,5587,5774,3507,6420,4193,4171,5038,5027,5113,5823,6723,2231,7139,7132,6776,3700,4881,5724,6305,6145,3196,12,10,6624,4876"
		data["ctl00$cphMain$hdProductType_2"] = "4883,2801,5890,3376,3134,6664,5984,7,4964,4957,5659,6058,7375,2813,5974,5894,6662,6364,6660,6115,7482,3217,6156,6867,5130,5128,3229,6103,6774,6903,2307,5579,7462,7372,3712,5765,6466,7116,3450,5103,22,5707,4130,4178,3440,17,14,6917,6091,7113,6778,5942,5698,5997,8,4,11,3538,2818,7181,2,3647,5373,24,5901,5877,7418,5917,13,5135,5052,7354,7356,2785,7383,2853,7020,2855,7414,6093,3637,3,28,27,2884,7027,7423,7109,6658,29,5684,2890,4173,7179,3550,3548,3020,3132,5583,7166,7489,6137,7458,5073,6393,7097,5243,6925,2928,3703,5374,5767,7464,7391,6029,31,5538,2938,5020,7157,7444,5780,3743,5204,7476,7480,33,7095,5567,9,34,5713,6977,5587,5325,7301,5774,3507,4879,5192,6420,4193,4171,3685,5038,5027,5113,5823,6723,2231,7139,5661,7132,6879,6776,6403,5597,3700,4881,5724,6143,5655,6784,6305,6145,3196,12,10,6624"
		data["ctl00$cphMain$hdProductType_3"] = "4883,7388,2801,3677,3672,5890,3376,3134,6664,5984,7,4964,4957,5659,6058,7130,7375,2813,5974,7368,5894,6445,6662,6364,6912,6449,6660,7313,6115,7482,3217,6156,6867,7293,7338,5130,5128,3229,6103,6774,6903,2307,4873,5579,7462,7372,3712,5765,7056,6466,7116,3450,5103,7071,22,5707,4130,5187,4178,3440,17,14,6917,6091,7113,6778,5942,5698,6801,5997,8,4,11,3538,2818,7181,2,3647,5373,7296,24,5901,7418,5917,13,5135,5052,7354,7356,2785,7383,5957,5014,6253,7434,2853,7020,2855,7414,6093,3637,3,28,27,2884,7027,7423,7109,7394,6658,29,7298,7409,5684,7291,5888,2890,4173,7179,7288,3550,3548,3020,7161,7452,3132,5583,7166,7489,6137,7485,7458,5073,6706,6393,7097,7364,5243,6925,2928,3703,5374,5767,7464,7391,6029,4880,31,5538,2938,5020,7157,7444,5780,3743,5204,6788,7476,7480,33,7095,5567,7107,7083,5326,9,34,5713,6977,5587,7301,5774,3507,7120,4879,5192,6956,7173,6420,4193,4171,7059,7441,6799,3685,5038,5027,5113,5823,6723,2231,7139,5661,7132,6879,6776,6403,7171,5597,7438,3700,4881,5724,6143,5655,6784,5755,6305,6145,3196,4100,6459,7404,12,10,6965,6721,6624,4876"
		data["ctl00$cphMain$hdProductType_5"] = ""
		data["ctl00$cphMain$hdProductType_4"] = "4883,7388,2801,3677,3672,5890,3376,3134,6664,5984,7,4964,4957,5659,6058,7130,7375,2813,5974,7368,5894,6662,6364,6912,6449,6660,7313,6115,7482,3217,6156,6867,7293,7338,5130,5128,3229,6103,6774,6903,2307,4873,5579,7462,7372,3712,5765,7056,6466,7116,3450,5103,7071,22,5707,4130,5187,4178,3440,17,14,6917,6091,7113,6778,5942,5698,6801,5997,8,4,11,3538,2818,7181,2,3647,5373,7296,24,7418,5917,13,5135,5052,7354,7356,2785,7383,5957,5014,6253,7434,2853,7020,2855,7414,6093,3637,3,28,27,2884,7027,7423,7109,7394,6658,29,7298,7409,5684,7291,5888,2890,4173,7179,7288,3550,3548,3020,7161,7452,3132,5583,7166,7489,6137,7485,7458,5073,6393,7097,7364,5243,6925,2928,3703,5374,5767,7464,7391,6029,31,5538,2938,5020,7157,7444,5780,3743,5204,6788,7476,7480,33,7095,5567,7107,7083,5326,9,34,5713,6977,5587,7301,5774,3507,7120,5192,6956,7173,6420,4193,4171,7059,7441,6799,3685,5038,5027,5113,5823,6723,2231,7139,5661,7132,6879,6776,6403,7171,5597,7438,3700,4881,5724,6143,5655,6784,5755,6305,6145,3196,4100,6459,7404,12,10,6965,6721,6624"
		data["ctl00$cphMain$HidWow"] = ""
		data["ctl00$cphMain$hidpowerlevel"] = "0"
		data["ctl00$cphMain$hidaccount"] = "1"
		data["__VIEWSTATEGENERATOR"] = clean(r.text).split("id=\"__VIEWSTATEGENERATOR\" value=\"")[1].split("\"")[0]

		HEADERS = {}
		HEADERS["Connection"] = "keep-alive"
		HEADERS["Cache-Control"] = "max-age=0"
		HEADERS["Origin"] = "https://www.playerauctions.com"
		HEADERS["Upgrade-Insecure-Requests"] = "1"
		HEADERS["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
		HEADERS["Content-Type"] = "application/x-www-form-urlencoded"
		HEADERS["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
		HEADERS["Referer"] = "https://www.playerauctions.com/member/offereditor/?type=sell"
		HEADERS["Accept-Encoding"] = "gzip, deflate, br"
		HEADERS["Accept-Language"] = "en-US,en;q=0.8"
		# HEADERS["Cookie"] = "isFirstVist=True; ASP.NET_SessionId=zul2gh35djjxe545nyuym1mv; GLOBAL_COOKIE_ID=e72dc594-d83f-4498-bd3b-c80771542836; CountryISOCodeSetting=US; channel=google.com; __atssc=google%3B1; km_ai=DKFiP0G%2BEmSZq7b5hu%2FD0xYQP3M%3D; incap_ses_209_140605=ZAyZdWsQ9nadqwcmKIXmApO1olcAAAAAot6sQHzZNtPHLmqQwFWqSA==; incap_ses_261_172126=2G1+JZYovn/DqavYm0OfA5W1olcAAAAAVxq3UltFV9Fiv8Hfs8KDuA==; km_lv=x; TimeoutFlag=true; CurrencyUnitSetting=USD; visid_incap_140605=hIHti4J6Sam6TN/yb4Cvj6JxmVcAAAAAQUIPAAAAAACCeDRTojkNj7eoTPDK7cug; incap_ses_261_140605=Nnr2E+WKVW+wK7rYm0OfA4O7olcAAAAA5Mgd+uGz0LN/6D6VDixn8A==; visid_incap_172126=OsuoZrvgT9mFJiG7HP952qNxmVcAAAAAQUIPAAAAAADfiaNRzZa8DKybhXdBW5q0; incap_ses_208_172126=bo2YRJnFAnFSjItqKffiAozKolcAAAAAqlPNwC8hlv7gHzQ5IfkP1g==; userSurveyCookie=CurUserNickName=Iggytoad&IsOperate=false; subTab=7; __utmt=1; _gat_UA-3406877-1=1; __asc=fa6bff1115653954ad576ef9a84; __auc=872f9fb31562f63ec1152f46ba1; timeZone=-300; __utma=260853068.873181648.1469673897.1469673897.1470281111.2; __utmb=260853068.84.10.1470281111; __utmc=260853068; __utmz=260853068.1469673897.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _ga=GA1.2.873181648.1469673897; __atuvc=33%7C30%2C84%7C31; __atuvs=57a2c0d41de1e182042; playerAuction=26986A289A4E1BF336505CFB213ABCE3CDBAD93B2391B97929B2A8828BABA493A565EEBCC1E0A70DBEE6D10AEC26F2BD6ABF75505EAEE9CEFA4EC77282D1487F6A43096F2AA0C9FA8E11841D4681515B48BDA505BF3E488E3DE26C9DBF80662BE3C4D2AF5AAA83FDEC9E75644B4A101BC50A829D2B09989000613498F2F20F5ED0557FA5875A6623DF0853D4F9A83DD65E83F69CD3E96EAD23654AA13168D236AD0063ADDDDE7BEE1AA1F17CAECC198D06EE7802B0852FFA7BDB20F6C69AC5D3C6177A7AC12DFA5F2D5E7C6124C0E6F436F013CE19FEF19F485587FF403133584CB4F9F5671DA971C335C34C68226BDE9463F3BC119CDF412C11C3A1F103D84858C4407B; kvcd=1470288678682; km_vs=1; km_uq=; __utmli=ctl00_cphMain_btnCreate"

		r = s.post(url=POST_URL, data=data, allow_redirects=True, headers=HEADERS, verify=True, proxies=PROXY)
		# assert r.url == POST_URL
		if r.url != POST_URL:
			print "[{}] Failed to post {}".format(get_time(), ACCOUNT_USERNAME)
			continue
		else:
			num_posted += 1
			print "[{}] Posted #{} - {} '{}'".format(get_time(), num_posted, ACCOUNT_USERNAME, TITLE)
			with open("ptc_accounts_posted.txt", "a") as f:
				f.write(ACCOUNT_USERNAME + "\n")
			import random
			delay = random.randint(20,40)
			if num_posted >= list_amount:
				print "[{}] Finished posting {} accounts".format(get_time(), list_amount)
				break
			print "[{}] Sleeping for {}s".format(get_time(), delay)
			sleep(delay)
		# print r.url
		# # with open("pa_post.html", "w") as f:
		# # 	f.write(clean(r.text))
	except Exception, e:
		print str(e), account