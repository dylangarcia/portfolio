import requests, sys, urllib2, gzip
from time import strftime, sleep
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from BeautifulSoup import BeautifulSoup
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import random

def get_time():
	return strftime("%H:%M:%S")

def log(data):
	print "[{}] {}".format(get_time(), data)

def clean(text):
	return ''.join([i if ord(i) < 128 else ' ' for i in text])

def relist(url, username, ACCOUNT_USERNAME, TITLE, BODY, __VIEWSTATE, __VIEWSTATEGENERATOR):
	data = {}
	data["__EVENTTARGET"] = ""
	data["__EVENTARGUMENT"] = ""
	data["__LASTFOCUS"] = ""
	data["__VIEWSTATE"] = __VIEWSTATE
	data["ctl00$WebUserControl5$hfAdvancedSearch"] = ""
	data["ctl00$WebUserControl5$hfHideMaster"] = ""
	data["ctl00$WebUserControl5$hiddUserAccount"] = username
	data["ctl00$cphMain$hfCDkeyId"] = "5325"
	data["ctl00$cphMain$hfGiftCardsId"] = "6672"
	data["ctl00$cphMain$rblProductType"] = "3"
	data["ctl00$cphMain$ddlGame"] = "7464"
	data["ctl00$cphMain$ddlGameCopy"] = ""
	data["ctl00$cphMain$txtCategoryId"] = "7466"
	data["ctl00$cphMain$txtLoginName2"] = ACCOUNT_USERNAME
	data["ctl00$cphMain$txtReLoginName2"] = ACCOUNT_USERNAME
	data["ctl00$cphMain$iptSteamID"] = "long-form SteamID"
	data["ctl00$cphMain$hidSteamResult"] = "fail"
	data["ctl00$cphMain$rbdurationlistForCurrency"] = "7"
	data["ctl00$cphMain$rbdurationlistForOther"] = "30"
	data["ctl00$cphMain$txtPrice"] = "5"
	data["ctl00$cphMain$rblEnableReduction"] = "1"
	data["ctl00$cphMain$txtWaitDays"] = "5"
	data["ctl00$cphMain$ListingPrice"] = "5"
	data["ctl00$cphMain$PayoutAmount"] = "3.41"
	data["ctl00$cphMain$rblist_1"] = "2"
	data["ctl00$cphMain$rblist_14"] = "2"
	data["ctl00$cphMain$rblDisburseDelayDays"] = "7"
	data["ctl00$cphMain$rblist_12"] = "0"
	data["ctl00$cphMain$rblist_2"] = "1"
	data["ctl00$cphMain$rblist_3"] = "1"
	data["ctl00$cphMain$rblist_4"] = "0"
	data["ctl00$cphMain$rblist_15"] = "1"
	data["ctl00$cphMain$rblist_6"] = "1"
	data["ctl00$cphMain$rblist_13"] = "0"
	data["ctl00$cphMain$rblist_7"] = "1"
	data["ctl00$cphMain$rblist_8"] = "1"
	data["ctl00$cphMain$rblist_9"] = "1"
	data["ctl00$cphMain$rblist_10"] = "1"
	data["ctl00$cphMain$DescribeMainCharacter$rblLandMount"] = "1"
	data["ctl00$cphMain$DescribeMainCharacter$rblFlyingMount"] = "1"
	data["ctl00$cphMain$DescribeMainCharacter$rblUnderwaterMount"] = "1"
	data["ctl00$cphMain$DescribeMainCharacter$ddlAmount"] = "0"
	data["ctl00$cphMain$DescribeMainCharacter$ddlAccountExpansion"] = "1"
	data["ctl00$cphMain$DescribeMainCharacter$hfQualityValue"] = "1"
	data["ctl00$cphMain$DescribeMainCharacter$hfQualityValue2"] = "1"
	data["ctl00$cphMain$DescribeMainCharacter$hfIsMainCharacter"] = "1"
	data["ctl00$cphMain$CharacterDescribeUCtrol1$hfQualityValue"] = "1"
	data["ctl00$cphMain$CharacterDescribeUCtrol1$hfQualityValue2"] = "1"
	data["ctl00$cphMain$CharacterDescribeUCtrol1$hfIsMainCharacter"] = "1"
	data["ctl00$cphMain$CharacterDescribeForGW2UCtrol1$hfQualityValue"] = "1"
	data["ctl00$cphMain$CharacterDescribeForGW2UCtrol1$hfQualityValue2"] = "1"
	data["ctl00$cphMain$CharacterDescribeForGW2UCtrol1$hfIsMainCharacter"] = "1"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame$ddlAmount"] = "0"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame$txtGameId"] = "7464"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame$hfQualityValue"] = "1"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame$hfQualityValue2"] = "1"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame$hfIsMainCharacter"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter1$rblLandMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter1$rblFlyingMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter1$rblUnderwaterMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter1$ddlAmount"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter1$hfQualityValue"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter1$hfQualityValue2"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter1$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$ddlAmount"] = "0"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$txtGameId"] = "7464"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$hfQualityValue"] = "1"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$hfQualityValue2"] = "1"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame1$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter2$rblLandMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter2$rblFlyingMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter2$rblUnderwaterMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter2$ddlAmount"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter2$hfQualityValue"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter2$hfQualityValue2"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter2$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$ddlAmount"] = "0"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$txtGameId"] = "7464"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$hfQualityValue"] = "1"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$hfQualityValue2"] = "1"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame2$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter3$rblLandMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter3$rblFlyingMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter3$rblUnderwaterMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter3$ddlAmount"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter3$hfQualityValue"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter3$hfQualityValue2"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter3$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$ddlAmount"] = "0"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$txtGameId"] = "7464"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$txtGameName"] = ""
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$hfQualityValue"] = "1"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$hfQualityValue2"] = "1"
	data["ctl00$cphMain$CharacterDescribeForSpecifyGame3$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter4$rblLandMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter4$rblFlyingMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter4$rblUnderwaterMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter4$ddlAmount"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter4$hfQualityValue"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter4$hfQualityValue2"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter4$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter5$rblLandMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter5$rblFlyingMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter5$rblUnderwaterMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter5$ddlAmount"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter5$hfQualityValue"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter5$hfQualityValue2"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter5$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter6$rblLandMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter6$rblFlyingMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter6$rblUnderwaterMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter6$ddlAmount"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter6$hfQualityValue"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter6$hfQualityValue2"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter6$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter7$rblLandMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter7$rblFlyingMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter7$rblUnderwaterMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter7$ddlAmount"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter7$hfQualityValue"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter7$hfQualityValue2"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter7$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter8$rblLandMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter8$rblFlyingMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter8$rblUnderwaterMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter8$ddlAmount"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter8$hfQualityValue"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter8$hfQualityValue2"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter8$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter9$rblLandMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter9$rblFlyingMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter9$rblUnderwaterMount"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter9$ddlAmount"] = "0"
	data["ctl00$cphMain$DescribeExtraCharacter9$hfQualityValue"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter9$hfQualityValue2"] = "1"
	data["ctl00$cphMain$DescribeExtraCharacter9$hfIsMainCharacter"] = "0"
	data["ctl00$cphMain$txtTitle"] = TITLE
	data["ctl00$cphMain$hfScreenshot"] = "/20160803/948eb74882b3396a4093e22e55c6ddeb.png"
	data["ctl00$cphMain$hfScreenshotUrl"] = "http://image.playerauctions.com/P/B/20160803/948eb74882b3396a4093e22e55c6ddeb.png"
	data["ctl00$cphMain$Editor1"] = BODY
	data["ctl00$cphMain$Editor1$ClientState"] = "ToggleBorder=b:true;FullPage=b:false"
	data["addImageTo"] = "on"
	data["ctl00$cphMain$rblDeliveryGuarantee"] = "6"
	data["ctl00$cphMain$hfCategoryId"] = "7466"
	data["ctl00$cphMain$txtDeliveryInstruction"] = "(optional)"
	data["ctl00$cphMain$chkAgreement"] = "on"
	data["ctl00$cphMain$btnCreate"] = "Create New For Sale Offer"
	data["ctl00$cphMain$hidProduct"] = "3"
	data["ctl00$cphMain$hidIsRecommend"] = "false"
	data["ctl00$cphMain$hdProductType_1"] = "4883,7388,2801,5890,3376,3134,6664,7,4964,4957,5659,6058,7375,2813,5974,5894,6662,6364,6660,7482,3217,5130,5128,3229,6774,2307,5579,7462,7372,3712,7116,3450,7071,22,4130,4178,3440,17,14,6917,6091,7113,6778,5942,5698,5997,8,4,11,3538,2818,7181,2,3647,5373,24,5901,7418,5917,13,5135,5052,2785,2853,2855,3,28,27,2884,7027,7423,7109,6658,29,2890,4173,7179,3550,3548,3020,7161,7452,3132,5583,7166,7489,7458,5073,6393,5243,6925,2928,3703,5374,5767,31,5538,2938,7157,7444,5780,3743,5204,33,7095,5567,9,34,5713,6977,5587,5774,3507,6420,4193,4171,5038,5027,5113,5823,6723,2231,7139,7132,6776,3700,4881,5724,6305,6145,3196,12,10,6624,4876"
	data["ctl00$cphMain$hdProductType_2"] = "4883,2801,5890,3376,3134,6664,5984,7,4964,4957,5659,6058,7375,2813,5974,5894,6662,6364,6660,6115,7482,3217,6156,6867,5130,5128,3229,6103,6774,6903,2307,5579,7462,7372,3712,5765,6466,7116,3450,5103,22,5707,4130,4178,3440,17,14,6917,6091,7113,6778,5942,5698,5997,8,4,11,3538,2818,7181,2,3647,5373,24,5901,5877,7418,5917,13,5135,5052,7354,7356,2785,7383,2853,7020,2855,7414,6093,3637,3,28,27,2884,7027,7423,7109,6658,29,5684,2890,4173,7179,3550,3548,3020,3132,5583,7166,7489,6137,7458,5073,6393,7097,5243,6925,2928,3703,5374,5767,7464,7391,6029,31,5538,2938,5020,7157,7444,5780,3743,5204,7476,7480,33,7095,5567,9,34,5713,6977,5587,5325,7301,5774,3507,4879,5192,6420,4193,4171,3685,5038,5027,5113,5823,6723,2231,7139,5661,7132,6879,6776,6403,5597,3700,4881,5724,6143,5655,6784,6305,6145,3196,12,10,6624"
	data["ctl00$cphMain$hdProductType_3"] = "4883,7388,2801,3677,3672,5890,3376,3134,6664,5984,7,4964,4957,5659,6058,7130,7375,2813,5974,7368,5894,6445,6662,6364,6912,6449,6660,7313,6115,7482,3217,6156,6867,7293,7338,5130,5128,3229,6103,6774,6903,2307,4873,5579,7462,7372,3712,5765,7056,6466,7116,3450,5103,7071,22,5707,4130,5187,4178,3440,17,14,6917,6091,7113,6778,5942,5698,6801,5997,8,4,11,3538,2818,7181,2,3647,5373,7296,24,5901,7418,5917,13,5135,5052,7354,7356,2785,7383,5957,5014,6253,7434,2853,7020,2855,7414,6093,3637,3,28,27,2884,7027,7423,7109,7394,6658,29,7298,7409,5684,7291,5888,2890,4173,7179,7288,3550,3548,3020,7161,7452,3132,5583,7166,7489,6137,7485,7458,5073,6706,6393,7097,7364,5243,6925,2928,3703,5374,5767,7464,7391,6029,4880,31,5538,2938,5020,7157,7444,5780,3743,5204,6788,7476,7480,33,7095,5567,7107,7083,5326,9,34,5713,6977,5587,7301,5774,3507,7120,4879,5192,6956,7173,6420,4193,4171,7059,7441,6799,3685,5038,5027,5113,5823,6723,2231,7139,5661,7132,6879,6776,6403,7171,5597,7438,3700,4881,5724,6143,5655,6784,5755,6305,6145,3196,4100,6459,7404,12,10,6965,6721,6624,4876"
	data["ctl00$cphMain$hdProductType_4"] = "4883,7388,2801,3677,3672,5890,3376,3134,6664,5984,7,4964,4957,5659,6058,7130,7375,2813,5974,7368,5894,6662,6364,6912,6449,6660,7313,6115,7482,3217,6156,6867,7293,7338,5130,5128,3229,6103,6774,6903,2307,4873,5579,7462,7372,3712,5765,7056,6466,7116,3450,5103,7071,22,5707,4130,5187,4178,3440,17,14,6917,6091,7113,6778,5942,5698,6801,5997,8,4,11,3538,2818,7181,2,3647,5373,7296,24,7418,5917,13,5135,5052,7354,7356,2785,7383,5957,5014,6253,7434,2853,7020,2855,7414,6093,3637,3,28,27,2884,7027,7423,7109,7394,6658,29,7298,7409,5684,7291,5888,2890,4173,7179,7288,3550,3548,3020,7161,7452,3132,5583,7166,7489,6137,7485,7458,5073,6393,7097,7364,5243,6925,2928,3703,5374,5767,7464,7391,6029,31,5538,2938,5020,7157,7444,5780,3743,5204,6788,7476,7480,33,7095,5567,7107,7083,5326,9,34,5713,6977,5587,7301,5774,3507,7120,5192,6956,7173,6420,4193,4171,7059,7441,6799,3685,5038,5027,5113,5823,6723,2231,7139,5661,7132,6879,6776,6403,7171,5597,7438,3700,4881,5724,6143,5655,6784,5755,6305,6145,3196,4100,6459,7404,12,10,6965,6721,6624"
	data["ctl00$cphMain$hidpowerlevel"] = "0"
	data["ctl00$cphMain$hidaccount"] = "1"
	data["__VIEWSTATEGENERATOR"] = __VIEWSTATEGENERATOR

	HEADERS = {}
	HEADERS["Connection"] = "keep-alive"
	HEADERS["Cache-Control"] = "max-age=0"
	HEADERS["Origin"] = "https://www.playerauctions.com"
	HEADERS["Upgrade-Insecure-Requests"] = "1"
	HEADERS["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"
	HEADERS["Content-Type"] = "application/x-www-form-urlencoded"
	HEADERS["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
	HEADERS["Referer"] = "https://www.playerauctions.com/member/offereditor/?type=sell"
	HEADERS["Accept-Encoding"] = "gzip, deflate, br"
	HEADERS["Accept-Language"] = "en-US,en;q=0.8"
	# HEADERS["Cookie"] = "isFirstVist=True; ASP.NET_SessionId=zul2gh35djjxe545nyuym1mv; GLOBAL_COOKIE_ID=e72dc594-d83f-4498-bd3b-c80771542836; CountryISOCodeSetting=US; channel=google.com; __atssc=google%3B1; km_ai=DKFiP0G%2BEmSZq7b5hu%2FD0xYQP3M%3D; incap_ses_209_140605=ZAyZdWsQ9nadqwcmKIXmApO1olcAAAAAot6sQHzZNtPHLmqQwFWqSA==; incap_ses_261_172126=2G1+JZYovn/DqavYm0OfA5W1olcAAAAAVxq3UltFV9Fiv8Hfs8KDuA==; km_lv=x; TimeoutFlag=true; CurrencyUnitSetting=USD; visid_incap_140605=hIHti4J6Sam6TN/yb4Cvj6JxmVcAAAAAQUIPAAAAAACCeDRTojkNj7eoTPDK7cug; incap_ses_261_140605=Nnr2E+WKVW+wK7rYm0OfA4O7olcAAAAA5Mgd+uGz0LN/6D6VDixn8A==; visid_incap_172126=OsuoZrvgT9mFJiG7HP952qNxmVcAAAAAQUIPAAAAAADfiaNRzZa8DKybhXdBW5q0; incap_ses_208_172126=bo2YRJnFAnFSjItqKffiAozKolcAAAAAqlPNwC8hlv7gHzQ5IfkP1g==; userSurveyCookie=CurUserNickName=Iggytoad&IsOperate=false; subTab=7; __utmt=1; _gat_UA-3406877-1=1; __asc=fa6bff1115653954ad576ef9a84; __auc=872f9fb31562f63ec1152f46ba1; timeZone=-300; __utma=260853068.873181648.1469673897.1469673897.1470281111.2; __utmb=260853068.84.10.1470281111; __utmc=260853068; __utmz=260853068.1469673897.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _ga=GA1.2.873181648.1469673897; __atuvc=33%7C30%2C84%7C31; __atuvs=57a2c0d41de1e182042; playerAuction=26986A289A4E1BF336505CFB213ABCE3CDBAD93B2391B97929B2A8828BABA493A565EEBCC1E0A70DBEE6D10AEC26F2BD6ABF75505EAEE9CEFA4EC77282D1487F6A43096F2AA0C9FA8E11841D4681515B48BDA505BF3E488E3DE26C9DBF80662BE3C4D2AF5AAA83FDEC9E75644B4A101BC50A829D2B09989000613498F2F20F5ED0557FA5875A6623DF0853D4F9A83DD65E83F69CD3E96EAD23654AA13168D236AD0063ADDDDE7BEE1AA1F17CAECC198D06EE7802B0852FFA7BDB20F6C69AC5D3C6177A7AC12DFA5F2D5E7C6124C0E6F436F013CE19FEF19F485587FF403133584CB4F9F5671DA971C335C34C68226BDE9463F3BC119CDF412C11C3A1F103D84858C4407B; kvcd=1470288678682; km_vs=1; km_uq=; __utmli=ctl00_cphMain_btnCreate"

	r = s.post(url=url, data=data, allow_redirects=True, headers=HEADERS, verify=True, proxies=PROXY)
	return (r.url, clean(r.text))

HEADERS = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}

HEADERS["Connection"] = "keep-alive"
HEADERS["Cache-Control"] = "max-age=0"
HEADERS["Origin"] = "https://www.playerauctions.com"
HEADERS["Upgrade-Insecure-Requests"] = "1"
HEADERS["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML] = like Gecko) Chrome/51.0.2704.103 Safari/537.36"
HEADERS["Content-Type"] = "application/x-www-form-urlencoded"
HEADERS["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
HEADERS["Referer"] = "https://www.playerauctions.com/member/signin/"
HEADERS["Accept-Encoding"] = "gzip, deflate, br"
HEADERS["Accept-Language"] = "en-US,en;q=0.8"
HEADERS["Cookie"] = "isFirstVist=True; ASP.NET_SessionId=zul2gh35djjxe545nyuym1mv; GLOBAL_COOKIE_ID=e72dc594-d83f-4498-bd3b-c80771542836; CountryISOCodeSetting=US; channel=google.com; __atssc=google%3B1; km_ai=DKFiP0G%2BEmSZq7b5hu%2FD0xYQP3M%3D; visid_incap_140605=hIHti4J6Sam6TN/yb4Cvj6JxmVcAAAAAQUIPAAAAAACCeDRTojkNj7eoTPDK7cug; incap_ses_209_140605=ZAyZdWsQ9nadqwcmKIXmApO1olcAAAAAot6sQHzZNtPHLmqQwFWqSA==; incap_ses_261_172126=2G1+JZYovn/DqavYm0OfA5W1olcAAAAAVxq3UltFV9Fiv8Hfs8KDuA==; km_lv=x; CurrencyUnitSetting=USD; visid_incap_172126=OsuoZrvgT9mFJiG7HP952qNxmVcAAAAAQUIPAAAAAADfiaNRzZa8DKybhXdBW5q0; incap_ses_208_172126=gz+MMVAZlmS1nEpqKffiAqm3olcAAAAAL9pWbznUR/UVQ9j0QixMvg==; __utmt=1; _gat_UA-3406877-1=1; userSurveyCookie=CurUserNickName=&IsOperate=false; __asc=fa6bff1115653954ad576ef9a84; __auc=872f9fb31562f63ec1152f46ba1; timeZone=-300; __utma=260853068.873181648.1469673897.1469673897.1470281111.2; __utmb=260853068.17.10.1470281111; __utmc=260853068; __utmz=260853068.1469673897.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __atuvc=33%7C30%2C16%7C31; __atuvs=57a2b59a5cc7941e00f; _ga=GA1.2.873181648.1469673897; kvcd=1470282406997; km_vs=1; km_uq=; incap_ses_261_140605=Nnr2E+WKVW+wK7rYm0OfA4O7olcAAAAA5Mgd+uGz0LN/6D6VDixn8A==; ___utmvmOYufwwF=TROLQlhXFfm; ___utmvbOYufwwF=hZu XhEOXalw: GtF; __utmli=ctl00_cphMian_btnSignIn"

import sys
username = "PA_USERNAME"#sys.argv[1]
password = "PA_PASSWORD"#sys.argv[2]
with open("login.txt", "r") as f:
	f_ = f.read().split(":")
	username = f_[0]
	password = f_[1]

PROXY = {}

SIGNIN_URL = "https://www.playerauctions.com/member/signin/"
s = requests.Session()
r = s.request(method="get", url=SIGNIN_URL, allow_redirects=True, headers=HEADERS, verify=False, proxies=PROXY)

while "__VIEWSTATE" not in r.text:
	r = s.request(method="get", url=SIGNIN_URL, allow_redirects=True, headers=HEADERS, verify=False, proxies=PROXY)
	log("Retrying signin")
	sleep(3)

data = {}
data["__VIEWSTATE"] = clean(r.text).split("id=\"__VIEWSTATE\" value=\"")[1].split("\"")[0]
data["__VIEWSTATEGENERATOR"] = clean(r.text).split("id=\"__VIEWSTATEGENERATOR\" value=\"")[1].split("\"")[0]
data["__LASTFOCUS"] = ""
data["__EVENTTARGET"] = ""
data["__EVENTARGUMENT"] = ""
data["ctl00$Top1$hfAdvancedSearch"] = ""
data["ctl00$Top1$hfHideMaster"] = ""
data["ctl00$Top1$hiddUserAccount"] = ""
data["ctl00$cphMian$txtUserID"] = username
data["ctl00$cphMian$txtPassword"] = password
data["ctl00$cphMian$btnSignIn"] = "Sign In"
r = s.post(url=r.url, data=data, allow_redirects=True, headers=HEADERS, verify=False, proxies=PROXY)
log(r.url)
log("Signed in as '{}'".format(username))

CANCEL_AMOUNT = int(sys.argv[1])
log("Only acting on {} offers".format(CANCEL_AMOUNT))

CANCEL_URLS = []

CANCEL_URL = "https://www.playerauctions.com/member/myoffer/?type=sell&status=1&categoryid=&gameid=&producttype=&offerstate=&page="
r = s.request(method="get", url=CANCEL_URL, allow_redirects=True, verify=False, proxies=PROXY)

bs = BeautifulSoup(clean(r.text))
NUM_OF_PAGES_ELEM = bs.find("span", {"id": "ctl00_cphMain_ctrlPages_lblPageInfo"})
if NUM_OF_PAGES_ELEM:
	NUM_OF_PAGES = NUM_OF_PAGES_ELEM.text.split("of")[1]
else:
	log("Couldn't find any pages for active offers")
	NUM_OF_PAGES = 0
# NUM_OF_PAGES = 0 #######CHANGE THISSSSSSSSSSSSSSSSSSSSSSSSSSSSS
log("Number of active pages: %s" % NUM_OF_PAGES)

for page_num in range(int(NUM_OF_PAGES), 0, -1):
	if len(CANCEL_URLS) >= CANCEL_AMOUNT:
		break
	r = s.request(method="get", url=CANCEL_URL + str(page_num), allow_redirects=True, verify=False, proxies=PROXY)
	bs = BeautifulSoup(clean(r.text))
	titles = []
	for a in bs.findAll("a", href=True, title=True):
		if "/offer/" in a["href"]:
			# log(a["title"])
			titles.append(a["title"])
	# log(bs.findAll("input", {"title": "Cancel"}))
	for elem in bs.findAll("input", {"title": "Cancel"}):
		if len(CANCEL_URLS) >= CANCEL_AMOUNT:
			break
		if "confirm" in elem["onclick"]:
			CANCEL_URLS.append(elem["onclick"].split("'")[3])

log("Found {} offers on {} pages".format(len(CANCEL_URLS), NUM_OF_PAGES))
# log(CANCEL_URLS)

RELIST_URL = "https://www.playerauctions.com/member/offereditor/?type=sell&offerid={}&action=copy"
RELIST_URLS = []

for url in CANCEL_URLS:
	id_ = url.split("offerid=")[1].split("&")[0]
	log("Cancelling offer {}".format(id_))
	r = s.request(method="get", url=url, allow_redirects=True, verify=False, proxies=PROXY)
	RELIST_URLS.append(RELIST_URL.format(id_))

# RELIST_URL = "https://www.playerauctions.com/member/myoffer/?type=sell&status=0&categoryid=&gameid=&producttype=&offerstate=&page="
# r = s.request(method="get", url=RELIST_URL, allow_redirects=True, verify=False, proxies=PROXY)
# bs = BeautifulSoup(clean(r.text))
# NUM_OF_PAGES_ELEM = bs.find("span", {"id": "ctl00_cphMain_ctrlPages_lblPageInfo"})
# if NUM_OF_PAGES_ELEM:
# 	NUM_OF_PAGES = NUM_OF_PAGES_ELEM.text.split("of")[1]
# else:
# 	NUM_OF_PAGES = 0
# log("Number of already cancelled pages: %s" % NUM_OF_PAGES)
# relist_len = len(RELIST_URLS)

# for page_num in range(int(NUM_OF_PAGES), 0, -1):
# 	r = s.request(method="get", url=RELIST_URL + str(page_num), allow_redirects=True, verify=False, proxies=PROXY)
# 	# log(r.url)
# 	bs = BeautifulSoup(clean(r.text))
# 	for elem in bs.findAll("a", {"target": "_blank"}):
# 		# if "id" not in elem: continue
# 		if "Re-list" in elem.text:
# 			id_ = elem["href"].split("offerid=")[1].split("&")[0]
# 			# log("Adding already cancelled offer '{}'".format(id_))
# 			RELIST_URLS.append(elem["href"])
# 			if len(RELIST_URLS) >= CANCEL_AMOUNT:
# 				break
# log("Added {} already cancelled offers".format(len(RELIST_URLS) - relist_len))


already_relisted_titles = []
with open("relisted.txt", "r") as f:
	for _ in f.read().split("\n"):
		if not _: continue
		already_relisted_titles.append(_)
log("Already relisted {} offers, skipping".format(len(already_relisted_titles)))
SHOULD_RELIST = "y" in sys.argv[2]
log("Relisting {} offers".format(len(RELIST_URLS)))
for url in RELIST_URLS:
	id_ = url.split("offerid=")[1].split("&")[0]
	try:
		r = s.request(method="get", url=url, allow_redirects=True, verify=False, proxies=PROXY)
		bs = BeautifulSoup(clean(r.text))
		post_username = bs.find("input", {"name": "ctl00$cphMain$txtLoginName2"})["value"]
		title = bs.find("input", {"name": "ctl00$cphMain$txtTitle"})["value"]
		body = bs.find("textarea", {"name": "ctl00$cphMain$Editor1"}).text
		__VIEWSTATE = clean(r.text).split("id=\"__VIEWSTATE\" value=\"")[1].split("\"")[0]
		__VIEWSTATEGENERATOR = clean(r.text).split("id=\"__VIEWSTATEGENERATOR\" value=\"")[1].split("\"")[0]
		if title in already_relisted_titles:
			log("Skipping '{}'; already relisted".format(title))
			continue
		if SHOULD_RELIST:
			data = relist("https://www.playerauctions.com/member/offereditor/?type=sell&offerid={}&action=copy".format(id_), username, post_username, title, body, __VIEWSTATE, __VIEWSTATEGENERATOR)
			log("Relisted '{}' with title '{}'".format(post_username, title))
		already_relisted_titles.append(title)
		
		with open("relisted.txt", "a") as f:
			f.write("{}:{}\n".format(post_username, post_username[::-1]))

		if SHOULD_RELIST:
			if url == RELIST_URLS[-1]: continue
			delay = random.randint(1, 5) * 60
			log("Sleeping for {} minutes".format(delay / 60))
			sleep(delay)
	except Exception, e:
		log("{} failed".format(id_) + " " + str(e) + " - " + type(e).__name__)

log("Relisted {} listings".format(len(already_relisted_titles)))