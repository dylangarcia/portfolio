player_auction.py inputs a list of accounts (user:pass) and automatically creates listings on PlayerAuctions.com. By hand, each account takes ~3-5 minutes to post, however, this can post once every 3 seconds.
player_auction_cancel.py takes those accounts posted and automatically relists them to keep them in the New queue.
In combined use, this boost sales by over 200%.