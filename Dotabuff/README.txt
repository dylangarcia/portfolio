##DotaBuffPy

```
Usage: heroStats.py profileID "Hero Name"
	   heroStats.py profileID "Hero Name" --rankedOnly
```

The **Profile ID** is the ID given to you by **dotabuff.com**.

####Example Output:
```
Hero Name: Clockwerk
Number of Games Played: 5
Win / Loss Ratio: 0.40
Average Score (K / D / A): 12.0 / 9.0 / 17.0
Best Game Score (K / D / A): 15 / 5 / 15
Average Game Duration (HH:MM:SS): 00:48:42
Highest Num of Kills: 18
Highest Num of Deaths: 13
Top 5 Most Common Items: Blade Mail (5), Aghanim's Scepter (4), Phase Boots (3), Boots of Travel (2), Mekansm (2)
Items With The Highest Win Rate: Boots of Travel (2), Shiva's Guard (1), Dust of Appearance (1)
Items With The Highest Loss Rate: Heart of Tarrasque (1), Magic Stick (1), Platemail (1)
```
