#title           :heroStats.py
#description     :Get hero stats from DotaBuff profile
#author          :Dylan Garcia
#date            :20140216
#version         :1.0
#usage           :python heroStats.py profileID "Hero Name" [--rankedOnly]
#notes           :GPL V3 Licensing
#python_version  :2.7.3 
#==============================================================================

from BeautifulSoup import BeautifulSoup
from numbers import Number
from collections import Counter
import urllib2, pprint, time, sys

profile_id = None
hero = None
ranked_only = False

if len(sys.argv) < 3:
	print "Usage: heroStats.py profileID \"Hero Name\""
	print "Usage: heroStats.py profileID \"Hero Name\" --rankedOnly"
	sys.exit(1)
else:
	profile_id = int(sys.argv[1])
	hero = str(sys.argv[2])
	ranked_match = len(sys.argv) == 4

matches = []
scores = []
times = []
items = []
winning_items = []
losing_items = []
wins = 0
losses = 0

profile_page = urllib2.urlopen("http://dotabuff.com/players/{}/matches".format(str(profile_id)))
soup = BeautifulSoup(profile_page, convertEntities=BeautifulSoup.HTML_ENTITIES)
last_page_text = [x['href'] for x in soup.findAll('a') if "?page=" in x['href']][-1] #Get the tag where you can click for the last page

last_page = "".join([last_page_text[-i] for i in xrange(1, 4) if last_page_text[-i].isdigit()][::-1]) #Start backwards until you hit a non-digit character and then reverse it (since it was backwards)

for index in xrange(1, int(last_page) + 1):
	match_page = urllib2.urlopen("http://dotabuff.com/players/{}/matches?page={}".format(str(profile_id), str(index)))
	soup = BeautifulSoup(match_page, convertEntities=BeautifulSoup.HTML_ENTITIES)
	page_matches = []

	for link in soup.findAll('a', href=True):
		if link.string is not None:
			if hero in link.string: #Find all matches on the page with the chosen hero
				page_matches.append(link['href']) #Add to the page_matches list

	for match in page_matches:
		parent_of_match = soup.find('tr', {'data-link-to': match}) #Get the top level of the match
		won_match = False
		ranked_match = False
		if parent_of_match.findChildren() is not None:
			for children in parent_of_match.findChildren():
				if children.string is not None:
					if "Ranked Match" in children.string:
						ranked_match = True
					if (ranked_only and ranked_match) or not ranked_only:
						if "Lost Match" in children.string:
							losses += 1
							won_match = False
						if "Won Match" in children.string:
							wins += 1
							won_match = True
						if " / " in children.string: #Format of Kills / Deaths / Assists includes /
							scores.append(children.string)
						if ":" in children.string and len(children.string) <= 7: #Looking for time stamps, and if the length is <= it's duration and not play time
							duration_of_game = children.string
							if duration_of_game.count(':') == 1:
								times.append("00:" + duration_of_game) #Make all times HH:MM:SS
							else:
								times.append(duration_of_game)
			if (ranked_only and ranked_match) or not ranked_only:
				for item in parent_of_match.findAll("img", {"class": "image-icon image-item"}): #Get all items from that match
					if won_match:
						winning_items.append(item['alt'])
					else:
						losing_items.append(item['alt'])


	matches += page_matches #Add this page's matches to a list of all of the matches (used later)

def convertToSeconds(time): #Converts HH:MM:SS string to seconds
	return int(time.split(":")[0]) * (60 * 60) + int(time.split(":")[1]) * 60 + int(time.split(":")[2])

items = winning_items + losing_items #Combine the items into one list for generic stats

time_seconds = [convertToSeconds(game_time) for game_time in times] #Change all times to seconds
avg_time = sum(time_seconds) / (len(time_seconds) if len(time_seconds) > 0 else 1) #Get the average game time in seconds
avg_time = time.strftime('%H:%M:%S', time.gmtime(avg_time)) #Change back to HH:MM:SS

kills = [int(score.split(" / ")[0]) for score in scores] #List of all of the kills
deaths = [int(score.split(" / ")[1]) for score in scores] #List of all of the deaths
assists = [int(score.split(" / ")[2]) for score in scores] #List of all of the assists

avg_kills = "%0.1f" % (sum(kills) / float(len(scores) if len(scores) > 0 else 1)) #Get the average # of kills
avg_deaths = "%0.1f" % (sum(deaths) / float(len(scores) if len(scores) > 0 else 1)) #Get the average # of deaths
avg_assists = "%0.1f" % (sum(assists) / float(len(scores) if len(scores) > 0 else 1)) #Get the average # of assists

win_loss_ratio = "%0.2f" % (float(wins)/(float(wins + losses) if wins + losses > 0 else 1)) #Get the Win/Loss ratio

top_5_items = Counter(items).most_common(5) #Use the Counter class to get the 5 most common items

zip_scores = [float(score[0]) / float(score[1] if score[1] != 0 else 1) for score in zip(kills, deaths)] #Make the two separate kills/deaths list as one with the format of [K1, D1], [K2, D2] and get the average
if len(zip_scores) > 0:
	index_of_best_game = zip_scores.index(max(zip_scores)) #Get the index of the best game so we can use it later on to get the number of kills & deaths
else:
	index_of_best_game = -1 #Set to a value it wont naturally be so we can check this later on

just_winning_items = [winning_item for winning_item in winning_items if winning_item not in losing_items] #Separate only winning items from winning & losing
top_winning_items = Counter(just_winning_items).most_common(5) #5 Most common winning only items
just_losing_items = [losing_item for losing_item in losing_items if losing_item not in winning_items] #Separate only losing items from winning & losing
top_losing_items = Counter(just_losing_items).most_common(5) #5 Most common losing only items

print ""
print "Hero Name: {hero}".format(hero=hero)
print "Number of Games Played: {num}".format(num=len(scores))
print "Win / Loss Ratio: {wlr}".format(wlr=win_loss_ratio)
print "Average Score (K / D / A): {k} / {d} / {a}".format(k=avg_kills, d=avg_deaths, a=avg_assists)
print "Best Game Score (K / D / A): {k} / {d} / {a}".format(k=kills[index_of_best_game] if index_of_best_game != -1 else 0, d=deaths[index_of_best_game] if index_of_best_game != -1 else 0, a=assists[index_of_best_game] if index_of_best_game != -1 else 0)
print "Average Game Duration (HH:MM:SS): {time}".format(time=avg_time)
print "Highest Num of Kills: {k}".format(k=max(kills) if len(kills) > 0 else 0)
print "Highest Num of Deaths: {d}".format(d=max(deaths) if len(deaths) > 0 else 0)
print "Top 5 Most Common Items: {itemList}".format(itemList=", ".join(["{} ({})".format(item, frequency) for item, frequency in top_5_items]))
print "Items With The Highest Win Rate: {winList}".format(winList=", ".join(["{} ({})".format(item, frequency) for item, frequency in top_winning_items]))
print "Items With The Highest Loss Rate: {lossList}".format(lossList=", ".join(["{} ({})".format(item, frequency) for item, frequency in top_losing_items]))
print ""

if False: #Debug raw data
	if len(scores) <= 30: #So you can actually see the output
		print "Raw Scores: "
		pprint.pprint(zip(kills, deaths, assists))

	pprint.pprint(Counter(just_winning_items).most_common(5))
	pprint.pprint(Counter(just_losing_items).most_common(5))
